<?php

// suppression
if(isset($_GET['delete'])){
    $id = $db->quote($_GET['delete']);
    $db->query("DELETE FROM reservation WHERE num_salle=$id");
    setFlash('La salle a été supprimée');
    header('Location:salle_user.php');
    die();
}


// affichage des reservation
$select = $db->query('SELECT * FROM reservation');
$reservations = $select->fetchAll();
?>


<!-- Affichage des reservations -->
<div class="row">
<?php foreach ($reservations as $reservation): ?>
    <div class="col-4">
    <div class="card-deck">
        <div class="card shadow-lg">
            <div class="card-title"><?= $reservation['nom_salle'] ?></div>
            <div class="card-body">
                <p class="card-text">
                <div>
                <?php if($salle['informatise']==1){ ?>
                    <img src="image/free-computeur.png"> 
                <?php } else {?>
                    <img src="image/busy-computeur.png">
                    <?php } ?>
                </div>
                <div style="text-align:center">Nombre de place disponible : <?= $salle['nombre_place'] ?></div>
                <div class="btn-group">
                <a href="vue/salle_edit.php?id=<?=$salle['num_salle'];?>" class="btn btn-light">Editer</a>
                <a href="?delete=<?=$salle['num_salle'];?>" class="btn btn-light" onclick="return confirm('Voulez-vous suprimer cette salle?');">Supprimer</a>
                <a href="?reservation=<?=$salle['num_salle'];?>" class="btn btn-light">réserver</a>
                </div>
                </p>
            </div>
        </div>
        </div>
    </div>
    <?php endforeach; ?> 
</div>
<!--  -->