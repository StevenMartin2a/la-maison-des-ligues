<?php 
include '../lib/includes.php'; 

// Editer une salle
if (isset($_POST['nom_salle']) && isset($_POST['nombre_place'])){
    $id = $db->quote($_GET['id']);
    $nom_salle=$db->quote($_POST['nom_salle']);
    $place=$db->quote($_POST['nombre_place']);
    $db->query("UPDATE salle SET nom_salle=$nom_salle, nombre_place=$place WHERE num_salle=$id ");
    setFlash('La salle à été modifiée');
    header('Location:../index.php');
    die();
}

include '../partials/header.php';
?>


<h1 id="title" class="shadow"> Editer une salle </h1>

<form action="#" method="post">

    <div class="form-group">
        <label for="nom_salle">Nom de la salle</label>
        <?=input('nom_salle') ?>
    </div>
    <div class="form-group">
        <label for="nombre_place">Nombre de place</label>
        <?= input('nombre_place') ?>
    </div>

    <button type="submit" class="btn btn-success">Enregistrer</button>
</form>

<?php include '../partials/footer.php';?>