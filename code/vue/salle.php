<?php

// suppression
if(isset($_GET['delete'])){
    $id = $db->quote($_GET['delete']);
    $db->query("DELETE FROM salle WHERE num_salle=$id");
    setFlash('La salle a été supprimée');
    header('Location:index.php');
    die();
}

// réservation
if (isset($_GET['reservation'])){
    $id = $db->quote($_GET['reservation']);
    $db->query("INSERT INTO reservation VALUES ('', '', '', '', '', '',$id)");
    setFlash('La salle a été réservée');
    header('Location:index.php');
    die();
}
// affichage des salles
$select = $db->query('SELECT * FROM salle');
$salles = $select->fetchAll();
?>

<div class="row">
    <div class="col-4">
        <button class="btn btn-success"><a href="vue/salle_add.php?id=<?=$salle['num_salle'];?>"> Ajouter une salle </a></button>
    </div>
</div>
<!-- Affichage des salles -->
<div class="row">
<?php foreach ($salles as $salle): ?>
    <div class="col-4">
    <div class="card-deck">
        <div class="card shadow-lg">
            <div class="card-title"><?= $salle['nom_salle'] ?></div>
            <div class="card-body">
                <p class="card-text">
                <div>
                <?php if($salle['informatise']==1){ ?>
                    <img src="image/free-computeur.png"> 
                <?php } else {?>
                    <img src="image/busy-computeur.png">
                    <?php } ?>
                </div>
                <div style="text-align:center">Nombre de place disponible : <?= $salle['nombre_place'] ?></div>
                <div class="btn-group">
                <a href="vue/salle_edit.php?id=<?=$salle['num_salle'];?>" class="btn btn-light">Editer</a>
                <a href="?delete=<?=$salle['num_salle'];?>" class="btn btn-light" onclick="return confirm('Voulez-vous suprimer cette salle?');">Supprimer</a>
                <a href="?reservation=<?=$salle['num_salle'];?>" class="btn btn-light">réserver</a>
                </div>
                </p>
            </div>
        </div>
        </div>
    </div>
    <?php endforeach; ?> 
</div>
<!--  -->