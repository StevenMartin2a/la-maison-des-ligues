<?php 
include '../lib/includes.php'; 

// Ajouter une salle
if (isset($_POST['nom_salle']) && isset($_POST['nombre_place'])){
    $nom_salle=$db->quote($_POST['nom_salle']);
    $place=$db->quote($_POST['nombre_place']);
    $db->query("INSERT INTO salle SET nom_salle=$nom_salle, nombre_place=$place");
    setFlash('La salle à été ajoutée');
    header('Location:../index.php');
    die();
}

include '../partials/header.php';
?>


<h1 id="title" class="shadow"> Ajouter une salle </h1>

<form action="#" method="post">

    <div class="form-group">
        <label for="nom_salle">Nom de la salle</label>
        <?= input('nom_salle') ?>
    </div>
    <div class="form-group">
        <label for="nombre_place">Nombre de place</label>
        <?= input('nombre_place') ?>
    </div>

    <button type="submit" class="btn btn-success">Enregistrer</button>
</form>

<?php include '../partials/footer.php';?>