<?php include '../lib/includes.php' ?>

<!doctype html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <title>La maison des ligues de normandie</title>
  </head>
  <body>
    
    <div class="container">

<h1 id="title" class="shadow-lg"> La maison des ligues de normandie </h1>
<button class="btn btn-danger"><a href="../logout.php"> Déconnexion </a></button>

<?php


// réservation
if (isset($_GET['reservation'])){
    $id = $db->quote($_GET['reservation']);
    $db->query("INSERT INTO reservation VALUES ('', '', '', '', '', '',$id)");
    setFlash('La salle a été réservée');
    header('Location:salle_user.php');
    die();
}
// affichage des salles
$select = $db->query('SELECT * FROM salle');
$salles = $select->fetchAll();
?>

<div class="row">
</div>
<!-- Affichage des salles -->
<div class="row">
<?php foreach ($salles as $salle): ?>
    <div class="col-4">
    <div class="card-deck">
        <div class="card shadow-lg">
            <div class="card-title"><?= $salle['nom_salle'] ?></div>
            <div class="card-body">
                <p class="card-text">
                <div>
                <?php if($salle['informatise']==1){ ?>
                    <img src="../image/free-computeur.png"> 
                <?php } else {?>
                    <img src="../image/busy-computeur.png">
                    <?php } ?>
                </div>
                <div style="text-align:center">Nombre de place disponible : <?= $salle['nombre_place'] ?></div>
                
                <a href="?reservation=<?=$salle['num_salle'];?>" class="btn btn-light">réserver</a>
                
                </p>
            </div>
        </div>
        </div>
    </div>
    <?php endforeach; ?> 
</div>
<!--  -->

</div>
</body>
</html>