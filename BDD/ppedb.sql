-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 04 juin 2019 à 16:47
-- Version du serveur :  10.1.33-MariaDB
-- Version de PHP :  7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ppedb`
--

-- --------------------------------------------------------

--
-- Structure de la table `ligues`
--

CREATE TABLE `ligues` (
  `num_ligue` int(11) NOT NULL,
  `nom_ligue` varchar(255) NOT NULL,
  `nom_responsable` varchar(255) NOT NULL,
  `sport` varchar(255) NOT NULL,
  `tel` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ligues`
--

INSERT INTO `ligues` (`num_ligue`, `nom_ligue`, `nom_responsable`, `sport`, `tel`, `id`) VALUES
(1, 'fccaen', 'philippe', 'foot', 0, 0),
(2, 'caen athlé', 'george', 'athlétisme', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `num_reservation` int(11) NOT NULL,
  `date_demande` date NOT NULL,
  `debut_reservation` datetime NOT NULL,
  `fin-reservation` datetime NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `id` int(250) NOT NULL,
  `num_salle` int(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`num_reservation`, `date_demande`, `debut_reservation`, `fin-reservation`, `commentaire`, `id`, `num_salle`) VALUES
(18, '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 2, 6),
(17, '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 2, 5),
(16, '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 3),
(15, '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 5),
(14, '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `num_salle` int(250) NOT NULL,
  `nom_salle` varchar(255) NOT NULL,
  `nombre_place` int(255) NOT NULL,
  `informatise` tinyint(1) NOT NULL,
  `indisponible` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`num_salle`, `nom_salle`, `nombre_place`, `informatise`, `indisponible`) VALUES
(1, 'Salle 1', 20, 0, 0),
(2, 'Salle 2', 30, 0, 0),
(3, 'Salle 3', 30, 0, 0),
(4, 'Salle 4', 30, 0, 0),
(5, 'Salle 5', 30, 0, 0),
(6, 'Salle 6', 30, 0, 0),
(7, 'Salle 7', 0, 0, 0),
(8, 'Salle 8', 0, 0, 0),
(9, 'Salle 9', 0, 0, 0),
(10, 'Salle 10', 0, 0, 0),
(11, 'Salle 11', 0, 1, 0),
(12, 'Salle 12', 0, 1, 0),
(13, 'Salle 13', 0, 1, 0),
(14, 'Salle 14', 0, 1, 0),
(15, 'Salle 15', 0, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `admin`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1),
(2, 'user', '12dea96fec20593566ab75692c9949596833adc9', 0),
(3, 'JuryAdmin', 'baf01dd27ab17d0e76f3493066e16d57dc1fac33', 1),
(4, 'JuryUser', 'e1de94130cf9bd0c416274266c8decd6e0c38f1f', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ligues`
--
ALTER TABLE `ligues`
  ADD PRIMARY KEY (`num_ligue`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`num_reservation`),
  ADD KEY `num_ligue` (`id`),
  ADD KEY `num_salle` (`num_salle`),
  ADD KEY `num_reservation` (`num_reservation`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`num_salle`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ligues`
--
ALTER TABLE `ligues`
  MODIFY `num_ligue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `num_reservation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `num_salle` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
